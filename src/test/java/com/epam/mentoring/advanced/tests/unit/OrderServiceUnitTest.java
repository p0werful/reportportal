package com.epam.mentoring.advanced.tests.unit;

import com.epam.mentoring.advanced.app.bo.DiscountUtility;
import com.epam.mentoring.advanced.app.bo.OrderService;
import com.epam.mentoring.advanced.app.bo.ShoppingCart;
import com.epam.mentoring.advanced.app.bo.UserAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class OrderServiceUnitTest {
    @Mock
    DiscountUtility discountUtility;

    @InjectMocks
    OrderService orderService;
    UserAccount user;

    @BeforeEach
    public void setUp() {
        user = new UserAccount("John", "Smith", "1990/10/10", new ShoppingCart(new ArrayList<>()));
    }

    @Test
    void getOrderPriceWithoutProducts() {
        Mockito.when(discountUtility.calculateDiscount(user)).thenReturn(3.0);
        double totalPrice = orderService.getOrderPrice(user);
        Mockito.verify(discountUtility, times(1)).calculateDiscount(user);
        Mockito.verifyNoMoreInteractions(discountUtility);
        assertEquals(-3.0, totalPrice, 0.001);
    }
}
