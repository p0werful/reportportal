package com.epam.mentoring.advanced.tests.ui.selenide;

import com.epam.mentoring.advanced.data.ExcelDataProviders;
import com.epam.mentoring.advanced.hooks.HooksSelenide;
import com.epam.mentoring.advanced.listeners.SelenideListener;
import com.epam.mentoring.advanced.steps.selenide.CommonStepsSelenide;
import com.epam.mentoring.advanced.steps.selenide.FiltersStepsSelenide;
import com.epam.mentoring.advanced.steps.selenide.LaunchesStepsSelenide;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({SelenideListener.class})
public class AdvancedFiltersTestSelenide extends HooksSelenide {
    FiltersStepsSelenide filtersStepsSelenide = new FiltersStepsSelenide();
    CommonStepsSelenide commonStepsSelenide =new CommonStepsSelenide();
    LaunchesStepsSelenide launchesStepsSelenide = new LaunchesStepsSelenide();

    @BeforeMethod
    public void openFiltersPage() {
        commonStepsSelenide.goToFiltersPage();
    }

    @Test(description = "Add new filter", dataProvider = "dataFromExcel", dataProviderClass = ExcelDataProviders.class)
    public void addNewFilter(String filterName, String filterOptionName, String condition, String value) {
        commonStepsSelenide.goToLaunchesPage();
        launchesStepsSelenide.clickAddFilterButton();
        launchesStepsSelenide.clickMoreButton();
        launchesStepsSelenide.selectFilterOption(filterOptionName);
        launchesStepsSelenide.specifyFilterOption(filterOptionName, condition, value);
        launchesStepsSelenide.saveFilter();
        launchesStepsSelenide.enterFilterName(filterName);
        launchesStepsSelenide.clickAddButton();
        commonStepsSelenide.goToFiltersPage();
        filtersStepsSelenide.verifyThatFilterHasSpecifiedOptions(filterName, filterOptionName, condition, value);
    }

    @Test(dependsOnMethods = "addNewFilter", description = "Search for filter", dataProvider = "dataFromExcel", dataProviderClass = ExcelDataProviders.class)
    public void searchForFilter(String filterName, String filterOptionName, String condition, String value) {
        filtersStepsSelenide.searchForFilter(filterName);
        filtersStepsSelenide.verifyThatFilterHasSpecifiedOptions(filterName, filterOptionName, condition, value);
    }

    @Test(dependsOnMethods = "searchForFilter", description = "Delete created filter", dataProvider = "dataFromExcel", dataProviderClass = ExcelDataProviders.class)
    public void deleteCreatedFilter(String filterName, String filterOptionName, String condition, String value) {
        filtersStepsSelenide.deleteFilterFromTheList(filterName);
        filtersStepsSelenide.verifyThatFilterIsNotInTheList(filterName);
    }
}
