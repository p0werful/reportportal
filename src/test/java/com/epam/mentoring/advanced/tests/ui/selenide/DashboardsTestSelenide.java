package com.epam.mentoring.advanced.tests.ui.selenide;

import com.epam.mentoring.advanced.hooks.HooksSelenide;
import com.epam.mentoring.advanced.listeners.SelenideListener;
import com.epam.mentoring.advanced.steps.selenide.CommonStepsSelenide;
import com.epam.mentoring.advanced.steps.selenide.DashboardsStepsSelenide;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({SelenideListener.class})
public class DashboardsTestSelenide extends HooksSelenide {
    DashboardsStepsSelenide dashboardsStepsSelenide = new DashboardsStepsSelenide();
    CommonStepsSelenide commonStepsSelenide = new CommonStepsSelenide();

    @BeforeMethod
    public void openDashboardsPage() {
        commonStepsSelenide.goToDashboardsPage();
        dashboardsStepsSelenide.selectDashboardFromTheList("DEMO DASHBOARD");
    }

    @Test
    public void openDashboard() {
        dashboardsStepsSelenide.verifyThatDashboardIsOpened("DEMO DASHBOARD");
    }

    @Test
    public void resizeBigger() {
        dashboardsStepsSelenide.changeWidgetSize("LAUNCH STATISTICS AREA", 100,100);
        dashboardsStepsSelenide.verifyThatWidgetSizeIsChanged();
        dashboardsStepsSelenide.verifyThatWidgetLocationIsChanged();
    }

    @Test(dependsOnMethods = "resizeBigger")
    public void resizeSmaller() {
        dashboardsStepsSelenide.changeWidgetSize("LAUNCH STATISTICS AREA", -100,-100);
        dashboardsStepsSelenide.verifyThatWidgetSizeIsChanged();
        dashboardsStepsSelenide.verifyThatWidgetLocationIsChanged();
    }

    @Test
    public void dragAndDrop() {
        dashboardsStepsSelenide.moveWidget("LAUNCH STATISTICS AREA", 100, 0);
        dashboardsStepsSelenide.verifyThatWidgetLocationIsChanged();
    }
    @Test(dependsOnMethods = "dragAndDrop")
    public void dragAndDropBack() {
        dashboardsStepsSelenide.moveWidget("LAUNCH STATISTICS AREA", -100, 0);
        dashboardsStepsSelenide.verifyThatWidgetLocationIsChanged();
    }
}
