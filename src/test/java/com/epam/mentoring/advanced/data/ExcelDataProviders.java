package com.epam.mentoring.advanced.data;

import com.epam.mentoring.advanced.utils.ExcelReader;
import org.testng.annotations.DataProvider;

public class ExcelDataProviders {
    @DataProvider
    public Object[][] dataFromExcel() throws Exception {
        String path = "src/test/resources/data.xlsx";
        String sheetName = "Sheet1";
        ExcelReader excelReader = new ExcelReader(path, sheetName);
        return excelReader.getSheetData();
    }


}
