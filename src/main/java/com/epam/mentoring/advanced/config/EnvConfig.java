package com.epam.mentoring.advanced.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;
import org.aeonbits.owner.Config.LoadPolicy;

import static org.aeonbits.owner.Config.LoadType.MERGE;

@LoadPolicy(MERGE)
@Sources({"system:properties",
        "system:env",
        "classpath:configs/env/EnvConfig.properties"})

public interface EnvConfig extends Config {

    @Key("browser")
    @DefaultValue("chrome")
    String browser();
    @Key("host")
    @DefaultValue("localhost")
    String host();
    @Key("rp.endpoint")
    @DefaultValue("http://localhost:8080")
    String endpoint();

//    SAUCE LABS
    @Key("SAUCE_USER")
    String sauceUser();
    @Key("SAUCE_KEY")
    String sauceKey();
    @Key("platformName")
    @DefaultValue("Windows 10")
    String platformName();
    @Key("browserVersion")
    @DefaultValue("latest")
    String browserVersion();



    @Key("RP_USER")
    @DefaultValue("superadmin")
    String user();

    @Key("RP_PASSWORD")
    @DefaultValue("erebus")
    String password();

    @Key("RP_USER_2")
    @DefaultValue("default")
    String user2();

    @Key("RP_PASSWORD_2")
    @DefaultValue("1q2w3e")
    String password2();

    @Key("RP_USER_3")
    @DefaultValue("superadmin")
    String user3();

    @Key("RP_PASSWORD_3")
    @DefaultValue("erebus")
    String password3();

    @Key("RP_USER_4")
    @DefaultValue("default")
    String user4();

    @Key("RP_PASSWORD_4")
    @DefaultValue("1q2w3e")
    String password4();

    @Key("RP_USER_5")
    @DefaultValue("default")
    String user5();

    @Key("RP_PASSWORD_5")
    @DefaultValue("1q2w3e")
    String password5();

    @Key("SELENIDE_USER")
    @DefaultValue("default")
    String selenideUser();

    @Key("SELENIDE_PASSWORD")
    @DefaultValue("1q2w3e")
    String selenidePassword();

    @Key("HEADLESS")
    @DefaultValue("false")
    Boolean headless();

    @Key("THREADS")
    @DefaultValue("1")
    int threads();
}