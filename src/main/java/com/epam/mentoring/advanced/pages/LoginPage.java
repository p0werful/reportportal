package com.epam.mentoring.advanced.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class LoginPage extends BasePage {
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "[name='login']")
    private WebElement loginField;
    @FindBy(css = "[name='password']")
    private WebElement passwordField;
    @FindBy(css = "[type='submit']")
    private WebElement loginButton;

    public void login(String login, String password) {
        waiters.waitingForElementToBeClickable(loginButton);
        loginField.sendKeys(login);
        passwordField.sendKeys(password);
        loginButton.click();
        waiters.waitingForElementToBeGone(loginButton);
    }
}