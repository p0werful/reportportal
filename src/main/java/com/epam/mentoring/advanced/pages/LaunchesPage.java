package com.epam.mentoring.advanced.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class LaunchesPage extends BasePage {
    WebDriver driver;
    public LaunchesPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    @FindBy(xpath = "//div[text()='All launches']")
    private WebElement allLatestDropdown;

    @FindAll(@FindBy(xpath = "//div[@class='gridRow__grid-row-wrapper--1dI9K']"))
    private List<WebElement> launches;

    @FindBy(xpath = "//span[@class='ghostMenuButton__title--29_S9']")
    private WebElement actionsButton;

    @FindBy(xpath = "//div[@class='ghostMenuButton__menu-item---8Ftq']/span[text()='Delete']")
    private WebElement deleteAction;
    @FindBy(xpath = "//div[@class='checkboxHeaderCell__checkbox-header-cell--ICfjc']//input[@type='checkbox']")
    private WebElement checkboxAllLaunches;

    @FindBy(css = "[class='ghostButton__text--eUa9T']")
    private WebElement addFilterButton;

    @FindBy(xpath = "//div[@class='entitiesSelector__toggler--3ArsT']")
    private WebElement newFilterMoreButton;

    @FindBy(xpath = "//span[@class='filterItem__name--3ev0G']")
    private List<WebElement> filtersOnLaunches;

    @FindAll(@FindBy(xpath = "//span[@class='inputCheckbox__children-container--1lo3t']"))
    private List<WebElement> filterOptions;
    @FindAll(@FindBy(xpath = "//span[@class='fieldFilterEntity__entity-name--3nU4j']"))
    private List<WebElement> filterNames;
    @FindAll(@FindBy(xpath = "//input[@class='inputConditional__input--1ZRQE inputConditional__touched--3gPX5']"))
    private List<WebElement> filterFields;
    @FindAll(@FindBy(xpath = "//div[@class='inputConditional__conditions-block--2Hg_w']"))
    private List<WebElement> filterConditions;
    @FindAll(@FindBy(xpath = "//div[@class='inputConditional__condition--3BpCh']"))
    private List<WebElement> filterConditionsExpanded;
    @FindBy(xpath = "//button[@title='Save']")
    private WebElement saveFilterButton;

    //Add filter window
    @FindBy(xpath = "//input[@placeholder='Enter filter name']")
    private WebElement filterNameField;
    @FindBy(xpath = "//button[text()='Add']")
    private WebElement addButton;

    public void deleteAllLaunches() {
        if (checkIfLaunchesPresent()) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].click();", checkboxAllLaunches);
            js.executeScript("arguments[0].click();", actionsButton);
            waiters.waitingForElementToBeClickable(deleteAction);
            deleteAction.click();
            waiters.waitingForElementToBeClickable(confirmDeleteButton);
            confirmDeleteButton.click();
            waiters.waitingForElementToBeGone(confirmDeleteButton);
            waiters.waitingForElementToBeDisplayed(notificationForDeleteLaunches);
            waiters.waitingForElementToBeGone(notificationForDeleteLaunches);
            logger.info("All launches were deleted");
        } else {
            logger.info("No launches were deleted");
        }
    }

    public boolean checkIfLaunchesPresent() {
        return !launches.isEmpty();
    }

    public boolean isFilterDisplayedOnLaunches(String filterName) {
        for (WebElement filterOnLaunches:
                filtersOnLaunches) {
            if (filterOnLaunches.getText().contentEquals(filterName)) {
                return true;
            }
        }
        return false;
    }
    public void clickAddButton() {
        waiters.waitingForElementToBeClickable(addButton);
        addButton.click();
        waiters.waitingForElementToBeGone(addButton);
    }
    public void enterFilterName(String filterName) {
        waiters.waitingForElementToBeDisplayed(filterNameField);
        filterNameField.clear();
        filterNameField.sendKeys(filterName);
        logger.info("Entered the following filter name: {}", filterName);
    }

    public void clickSaveFilterButton() {
        waiters.waitingForElementToBeClickable(saveFilterButton);
        saveFilterButton.click();
    }
    public void specifyFilterOption(String filterOptionName, String condition, String value) {
        for (WebElement filterName:
                filterNames) {
            if (filterName.getText().contentEquals(filterOptionName)) {
                int indexOfElement = filterNames.indexOf(filterName);
                filterConditions.get(indexOfElement).click();
                for (WebElement filterCondition:
                        filterConditionsExpanded) {
                    if (filterCondition.isDisplayed() && filterCondition.getText().contentEquals(condition.toLowerCase())) {
                        filterCondition.click();
                    }
                }
                filterFields.get(indexOfElement).sendKeys(value);
            }
        }
    }

    public void selectFilterOptionFromTheList(String filterOptionName) {
        for (WebElement filterOption:
                filterOptions) {
            if (filterOption.getText().contentEquals(filterOptionName)) {
                filterOption.click();
                BasePage.logger.info("Selected the following filter option: {}", filterOption);
            }
        }
    }
    public void clickAddFilterButton() {
        addFilterButton.click();
    }
    public boolean isAllLatestDropdownDisplayed() {
        waiters.waitingForElementToBeDisplayed(allLatestDropdown);
        return allLatestDropdown.isDisplayed();
    }
    public void clickMoreButton() {
        waiters.waitingForElementToBeClickable(newFilterMoreButton);
        newFilterMoreButton.click();
    }
}
