package com.epam.mentoring.advanced.pages;

import com.epam.mentoring.advanced.utils.Waiters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
    public static final Logger logger = LogManager.getLogger(BasePage.class);
    protected Waiters waiters;
    protected Actions action;
    public BasePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        waiters = new Waiters(driver, 2L);
        action = new Actions(driver);
    }
    @FindBy(xpath = "//button[@class='bigButton__big-button--ivY7j bigButton__color-tomato--Wvy5L']")
    protected WebElement confirmDeleteButton;

    @FindBy(xpath = "//div[@class='notificationItem__message-container--16jY2 notificationItem__success--Xv97a']//p[text()='Demo data has been generated']")
    protected WebElement notificationForDemoData;

    @FindBy(xpath = "//div[@class='notificationItem__message-container--16jY2 notificationItem__success--Xv97a']//p[text()='Launches were deleted']")
    protected WebElement notificationForDeleteLaunches;

    @FindBy(xpath = "//div[@class='notificationItem__message-container--16jY2 notificationItem__success--Xv97a']//p[text()='Filter has been updated!']")
    protected WebElement notificationForUpdateFilter;
}
