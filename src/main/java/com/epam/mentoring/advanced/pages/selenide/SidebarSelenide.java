package com.epam.mentoring.advanced.pages.selenide;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Condition.*;
public class SidebarSelenide extends BasePageSelenide {
    private final SelenideElement filtersSidebarButton = $("div.layout__sidebar-container--gX2bY a[href*='/filters']");
    private final SelenideElement launchesSidebarButton = $("div.layout__sidebar-container--gX2bY a[href*='/launches']");
    private final SelenideElement dashboardsSidebarButton = $("div.layout__sidebar-container--gX2bY a[href*='/dashboard']");
    private final SelenideElement settingsSidebarButton = $("div.layout__sidebar-container--gX2bY a[href*='/settings']");
    private final SelenideElement allLaunches = $("div.allLatestDropdown__selected-value--WN9ZG");
    private final SelenideElement filtersTitle = $("[title='Filters']");
    private final SelenideElement allDashboards = $("[title='All Dashboards']");

    public void goToFiltersPage() {
        LOGGER.info("Going to Filters Page");
        filtersSidebarButton.should(appear);
        filtersSidebarButton.click();
        filtersTitle.should(appear);
    }

    public void goToLaunchesPage() {
        LOGGER.info("Going to Launches Page");
        launchesSidebarButton.should(appear);
        launchesSidebarButton.click();
        allLaunches.should(appear);
    }

    public void goToSettingsPage() {
        LOGGER.info("Going to Settings Page");
        settingsSidebarButton.should(appear);
        settingsSidebarButton.click();
    }

    public void goToDashboardsPage() {
        LOGGER.info("Going to Dashboards Page");
        dashboardsSidebarButton.should(appear);
        dashboardsSidebarButton.click();
        allDashboards.should(appear);
    }
}

