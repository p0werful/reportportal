package com.epam.mentoring.advanced.pages.selenide;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class LaunchesPageSelenide extends BasePageSelenide{

    private final SelenideElement allLatestDropdown = $("div[text()='All launches']");
    private final ElementsCollection launches = $$("div.gridRow__grid-row-wrapper--1dI9K");
    private final SelenideElement addFilterButton = $("div.launchFiltersToolbar__add-filter-button--GqSO3 button");
    private final SelenideElement newFilterMoreButton = $("div.entitiesSelector__toggler--3ArsT");
    private final ElementsCollection filtersOnLaunches = $$("span.filterItem__name--3ev0G");
    private final ElementsCollection filterOptions = $$("span.inputCheckbox__children-container--1lo3t");
    private final ElementsCollection filterNames = $$("span.fieldFilterEntity__entity-name--3nU4j");
    private final ElementsCollection filterFields = $$("input.inputConditional__input--1ZRQE.inputConditional__touched--3gPX5");
    private final ElementsCollection filterConditions = $$("div.inputConditional__conditions-block--2Hg_w");
    private final ElementsCollection filterConditionsExpanded = $$("div.inputConditional__condition--3BpCh");
    private final SelenideElement saveFilterButton = $("button[title='Save']");
    private final SelenideElement filterNameField = $("input[placeholder='Enter filter name']");
    private final SelenideElement addButton = $("button.bigButton__color-booger--2IfQT");


    public boolean checkIfLaunchesPresent() {
        return !launches.isEmpty();
    }

    public boolean isFilterDisplayedOnLaunches(String filterName) {
        return filtersOnLaunches.find(Condition.text(filterName)).isDisplayed();
    }

    public void clickAddButton() {
        addButton.shouldBe(appear);
        addButton.click();
    }

    public void enterFilterName(String filterName) {
        filterNameField.should(appear);
        filterNameField.clear();
        filterNameField.sendKeys(filterName);
        LOGGER.info("Entered the following filter name: {}", filterName);
    }

    public void clickSaveFilterButton() {
        saveFilterButton.should(appear);
        saveFilterButton.click();
    }

    public void specifyFilterOption(String filterOptionName, String condition, String value) {
        filterConditions.
                get(filterNames.indexOf(filterNames.find(exactText(filterOptionName)).shouldBe(visible))).
                click();
        filterConditionsExpanded.
                filterBy(visible).find(exactText(condition.toLowerCase())).shouldBe(visible).
                click();
        filterFields.
                get(filterNames.indexOf(filterNames.find(exactText(filterOptionName)).shouldBe(visible))).
                setValue(value);
    }

    public void selectFilterOptionFromTheList(String filterOptionName) {
        filterOptions.find(exactText(filterOptionName)).click();
        LOGGER.info("Selected the following filter option: {}", filterOptionName);
    }
    public void clickAddFilterButton() {
        addFilterButton.should(appear);
        addFilterButton.click();
    }

    public boolean isAllLatestDropdownDisplayed() {
        return allLatestDropdown.isDisplayed();
    }

    public void clickMoreButton() {
        newFilterMoreButton.should(appear);
        newFilterMoreButton.click();
    }
}
