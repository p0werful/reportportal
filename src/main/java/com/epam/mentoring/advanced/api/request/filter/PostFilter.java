package com.epam.mentoring.advanced.api.request.filter;

import com.epam.mentoring.advanced.api.dtos.PostFilterDto;
import com.epam.mentoring.advanced.api.request.BaseRequest;

public class PostFilter extends BaseRequest {
    public PostFilter(PostFilterDto dto) {
        method = "POST";
        url = "/filter/";
        body = dto;
    }
}
