package com.epam.mentoring.advanced.api.request.filter;

import com.epam.mentoring.advanced.api.dtos.PostFilterDto;
import com.epam.mentoring.advanced.api.request.BaseRequest;

public class PutFilter extends BaseRequest {
    public PutFilter(PostFilterDto dto, int id) {
        method = "PUT";
        url = "/filter/" + id;
        body = dto;
    }
}
