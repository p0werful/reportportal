package com.epam.mentoring.advanced.api.controllers;

import com.epam.mentoring.advanced.api.dtos.PostFilterDto;
import com.epam.mentoring.advanced.api.mappers.FilterMapper;
import com.epam.mentoring.advanced.api.models.filter.Filter;
import com.epam.mentoring.advanced.api.models.filter.FilterManager;
import com.epam.mentoring.advanced.api.request.filter.*;
import io.restassured.response.ValidatableResponse;

import static io.restassured.RestAssured.given;

public class FilterController {



    public ValidatableResponse getFilters() {
        return new GetFilters().execute();
    }
    public ValidatableResponse getFiltersNegative() {
        return new GetFilters().execute();
    }

    public ValidatableResponse getFilterById(int id) {
        return new GetFilterById(id).execute();
    }

    public ValidatableResponse postFilter(Filter filter) {
        PostFilterDto postFilterDto = FilterMapper.mapToPostDto(filter);
        return new PostFilter(postFilterDto).execute();

    }

    public ValidatableResponse putFilter(Filter filter) {
        PostFilterDto postFilterDto = FilterMapper.mapToPostDto(filter);
        return new PutFilter(postFilterDto, filter.getId()).execute();
    }

    public ValidatableResponse deleteFilter(int id) {
        return new DeleteFilter(id).execute();
    }


}
