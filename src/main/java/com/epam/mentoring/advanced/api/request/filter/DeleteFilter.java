package com.epam.mentoring.advanced.api.request.filter;

import com.epam.mentoring.advanced.api.request.BaseRequest;

public class DeleteFilter extends BaseRequest {
    public DeleteFilter(int id) {
        method = "DELETE";
        url = "/filter/" + id;
    }
}
